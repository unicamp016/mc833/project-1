\section{Método Experimental}\label{sec:method}
  Para medir o desempenho da comunicação cliente/servidor, foram coletadas \iterations~amostras do tempo total de consulta/atualização vistas pelo cliente ($T_t$) e do tempo de processamento para cada operação do ponto de vista do servidor ($T_p$). Como o tempo de processamento no servidor não está relacionado ao protocolo de comunicação, esse tempo é subtraido do tempo total do ponto de vista do cliente para obter uma estimativa do tempo de comunicação ($T_c = T_t - T_p$).

  As amostras foram coletadas sem concorrência, ou seja, havia um único cliente fazendo as requisições. Além disso, os testes foram feitos com conexões separadas. Isto é, para cada conexão TCP, é feita uma única requisição seguida pelo comando 0 para terminar a conexão. O sistema foi testado de maneira automatizada, onde um arquivo com os comandos desejados é redirecionado para a entrada padrão do cliente.

  Todas as amostras foram coletadas em uma LAN, com os computadores a uma distância de aproximadamente 1 metro e conectados entre si através de uma rede sem fio. A especifação das máquinas usadas para realizar os testes está indicada na Tabela~\ref{tab:pc-stats}. Como o banco de dados está armazenado no servidor, devemos levar em conta a especificação do servidor ao avaliar seu desempenho.

  \begin{table}[h!]
      \begin{center}
          \caption{Especificação das máquinas do cliente e servidor usadas para realizar o experimento.}\label{tab:pc-stats}
          \begin{tabular}{c|c|c}
          Tipo & Cliente & Servidor\\
          \hline
          CPU & Intel Pentium Dual-Core & Intel Core i5-7300HQ \\
          Clock & 2.20 GHz & 2.50GHz \\
          Cores & 2 & 4 \\
          RAM & 3GB 800MHz & 8GB 2400MHz \\
          Disco secundário & Seagate HDD SATA 6GB & SSD SATA 6GB \\
          Protocolo Wi-Fi & 802.11b/g & 802.11ac \\
          \end{tabular}
      \end{center}
  \end{table}


\section{Resultados}
  Os resultados dos experimentos detalhados na Seção~\ref{sec:method} estão dispostos na Tabela~\ref{tab:metrics}. A partir das medidas dos tempos $T_c$, $T_p$ e $T_t$, foram gerados os gráficos das Figuras~\ref{fig:op1} a~\ref{fig:comm-stderr}. A seguir será apresentada uma discussão dos principais resultados dos experimentos.
  \subsection{Tamanho de Código}
    O sistema foi dividido em um arquivo principal para o servidor, \texttt{server.c}, um arquivo principal para o cliente, \texttt{client.c}, uma biblioteca de funções para manipulação do banco de dados, \texttt{util.c} e uma biblioteca de funções auxiliares para definir formato de mensagem usado na comunicação, \texttt{socket.c}. Além disso, utilizamos 2 headers para definição de estruturas, protótipos de funções e inclusão de bibliotecas do sistema.

    O arquivo principal do servidor possui 217 linhas. Porém, a biblioteca \texttt{util.c} também é parte integrante do servidor, e é o maior arquivo, com 709 linhas. O cliente tem um tamanho parecido com o servidor básico, com 221 linhas. Por último, a biblioteca \texttt{socket.c} tem 75 linhas.

    Ao todo, o sistema tem 1222 linhas de código, sendo 513 delas em arquivos relacionados diretamente com a comunicação via rede. Por organização e questões estilísticas, uma grande parte das linhas é composta de comentários, linhas em branco e linhas de organização, como abertura ou fechamento de chaves.

    \subsection{Discussão}

    O tempo de resposta para uma requisição é uma variável aleatório contínua. O nosso objetivo é estimar a média da variável e encontrar o intervalo de confiança para essa estimativa da média. Usando o Teorema do Limite Central e assumindo uma distribuição normal, podemos estimar o valor da média e o erro padrão usando um conjunto de amostras através das Equações~\ref{eq:sample-mean} e~\ref{eq:sample-stdev}.

    \begin{equation}\label{eq:sample-mean}
        \mu_x = \frac{1}{n} \sum_{i=1}^n x_i
    \end{equation}

    \begin{equation}\label{eq:sample-stdev}
        \sigma_x = \frac{\sigma}{\sqrt{n}}
    \end{equation}

    Entretanto, como pode ser visto na Equação~\ref{eq:sample-stdev}, é necessário conhecer o desvio padrão da população ($\sigma$) para encontrar o erro padrão ($\sigma_x$). É claro que no caso dos tempos de resposta das requisições, esse parâmetro da população é desconhecido. Dessa forma, podemos optar por usar uma distribuição t de Student para computar o intervalo de confiança, que não requer o conhecimento prévio do desvio padrão da população, ou podemos usar uma estimativa para o desvio padrão da média. Por simplicidade, optamos pela segunda opção, onde usamos um estimador não enviesado para variância, do qual obtemos o erro padrão corrigido que é dada pela Equação~\ref{eq:unbiased-sample-stdev}~\cite{walpole}.

    \begin{equation}\label{eq:unbiased-sample-stdev}
        s_x = \sqrt{\frac{1}{n-1} \sum_{i=1}^n (x_i - \mu_x)^2}
    \end{equation}

    Usando a nossa estimativa para o erro padrão ($s_x$), podemos encontrar um intervalo de confiança de 95\% da média, isto é, um intervalo que tem 95\% de chance de conter a média real da população. Através da tabela para a função acumulada da distribuição normal, identificamos que o $Z$-score correspondente a esse intervalo de confiança é $1,96$. Portanto, podemos dizer que o intervalo $[\mu-1,96s_x, \mu+1,96s_x]$ tem 95\% de chance de conter a média da população. Os tempos $T_t$, $T_p$ e $T_c$ de cada operação estão dipostos na Tabela~\ref{tab:metrics} com a média, erro padrão e intervalo de confiança para cada variável.

    Pode-se observar que o tempo de comunicação para todas as operações é aproximadamente igual ao tempo total, já que o tempo gasto buscando os dados no servidor é praticamente desprezível ($\sim1$ms) comparado ao tempo gasto transmitindo-os pela rede. Isso é devido ao fato que o banco está armazenado em um SSD que permite um acesso aleatório extremamente rápido e a rede está limitada pela interface sem fio do cliente, que suporta uma taxa de transferência de no máximo 54 Mbps. Além disso, o uso de uma rede sem fio em um laboratório com inúmeros dispositivos conectados introduziu uma variância muito alta nas amostras coletadas, que pode ser fruto da interferência ou congestionamento da rede. Por isso, muitos dos intervalos de confiança na tabela foram clipados com limite inferior em 0ms, já que o limite inferior do intervalo de confiança era negativo para algumas medidas, o que não faz sentido dado que estamos tratando de tempo.

    \begin{table}[h!]
        \begin{center}
            \caption{Estimativa do tempo médio do cliente, comunicação e processamento de cada requisição com um intervalo de confiança de 95\%.}\label{tab:metrics}
            \begin{tabular}{c|c|c|c|c}
            Operação & Tipo & $\mu_x$ (ms) & $s_x$ (ms) & IC de 95\% (ms)\\
            \hline
            \multirow{3}{*}{1} & $T_p$ & 0,557  & 0,065  & [0,430; 0,684]\\
                               & $T_c$ & 30,314 & 45,872 & [0,000; 120,223]\\
                               & $T_t$ & 30,871 & 45,877 & [0,000; 120,789]\\
            \hline
            \multirow{3}{*}{2} & $T_p$ & 0,701  & 0,094   & [0,517; 0,885]\\
                               & $T_c$ & 98,057 & 150,055 & [0,000; 392,165]\\
                               & $T_t$ & 98,758 & 150,056 & [0,000; 392,873]\\
            \hline
            \multirow{3}{*}{3} & $T_p$ & 5,253  & 4,665  & [0,000; 14,396]\\
                               & $T_c$ & 65,392 & 96,893 & [0,000; 255,303]\\
                               & $T_t$ & 70,645 & 97,626 & [0,000; 261,992]\\
            \hline
            \multirow{3}{*}{4} & $T_p$ & 0,700  & 0,455   & [0,000; 1,593]\\
                               & $T_c$ & 83,509 & 112,793 & [0,000; 304,584]\\
                               & $T_t$ & 84,209 & 112,809 & [0,000; 305,315]\\
            \hline
            \multirow{3}{*}{5} & $T_p$ & 4,954   & 1,043   & [2,910; 6,998]\\
                               & $T_c$ & 350,442 & 293,735 & [0,000; 926,164]\\
                               & $T_t$ & 355,396 & 293,596 & [0,000; 930,845]\\
            \hline
            \multirow{3}{*}{6} & $T_p$ & 1,322  & 0,245  & [0,842; 1,803]\\
                               & $T_c$ & 37,024 & 67,698 & [0,000; 169,712]\\
                               & $T_t$ & 38,346 & 67,722 & [0,000; 171,081]\\
            \end{tabular}
        \end{center}
    \end{table}

    Além de computar os intervalos para a nossa aproximação da média de cada variável, podemos também visualizar graficamente como as amostras se distribuem em torno da nossa estimativa. Como estamos assumindo uma distribuição normal para os dados, usamos a função de densidade de probabilidade dessa distribuição, dada pela Equação \ref{eq:normal-pdf}, para obter uma probabilidade relativa de cada amostra. Como trata-se de uma variável aleatório contínua, não faz sentido falarmos da probabilidade para amostras discretas, apenas para intervalos de valores. Dessa forma, a soma das probabilidades relativas de cada ponto não somam $1$, mas nos ajudam a ter uma intuição do quão provável é que uma nova amostra assuma esse valor.

    \begin{equation}\label{eq:normal-pdf}
        f(x) = \frac{1}{s_x \sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2s_x^2}}
    \end{equation}

    As Figuras~\ref{fig:op1} até~\ref{fig:op6} do Apêndice~\ref{sec:appendix} mostram o valor de $f(x)$ na Equação~\ref{eq:normal-pdf} para todas as amostras coletadas de $T_t$ e $T_c$, destacando em vermelho a área correspondente ao intervalo de confiança de 95\%. Note que para a maior parte das operações, há valores que destoam muito da média, como a medida de 400 ms na Figura~\ref{fig:op1-cli}, que está muito distante dos demais tempos $T_t$ da operação 1. Para $T_t$ da operação 1, a média vale 30,314 ms e o erro padrão 45,871 ms. Um erro padrão da mesma ordem que a média da amostra não nos permite estimar a média da população já que o intervalo de confiança de 95\% é largo demais para inferirmos algo. Como dito anteriormente, a presença desses pontos destoantes nas medidas são uma consequência do método experimental adotado, onde usou-se a rede sem fio de um laboratório com inúmeros dispositivos conectados para realizar a comunicação. Esse ambiente está sujeito a muita interferência e torna o tempo de entrega dos segmentos imprevisível.

	Por fim, os gráficos nas Figuras~\ref{fig:client-stderr} e~\ref{fig:comm-stderr} permitem-nos comparar o tempo médio e o intervalo de confiança entre as operações. Note que a operação 5, que lista todos os perfis do servidor, possui, em média, um tempo total e de comunicação muito maior que as demais. A operação 6, apesar de poder ser pesada, por precisar transferir uma imagem, tem uma média menor que as demais porque a maioria dos perfis no banco não têm imagens e os testes foram feitos para um mesmo perfil que não possui uma.

  \begin{figure}[!h]
    \centering
    \includegraphics[width=0.73\textwidth]{./images/client-stderr.pdf}
    \caption{Tempo de total do ponto de vista do cliente ($T_t$) médio com o intervalo de confiança de 95\% para cada operação.}\label{fig:client-stderr}
  \end{figure}

  \begin{figure}[!h]
    \centering
    \includegraphics[width=0.73\textwidth]{./images/communication-stderr.pdf}
    \caption{Tempo de comunicação ($T_c$) médio com o intervalo de confiança de 95\% para cada operação.}\label{fig:comm-stderr}
  \end{figure}
