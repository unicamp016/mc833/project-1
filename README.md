# Setup instructions

### Package installation

Install mysql client and server
```
sudo apt-get install mysql-server -y
```

Install mysql libs
```
sudo apt-get install libmysqlclient-dev -y
```

### MySQL config
Start an interactive session
```
sudo mysql -u root`
```

Now from inside the interactive session, run:
```
> drop user 'root'@'localhost';
> create user 'root'@'localhost';
> grant all privileges on *.* to 'root'@'localhost' with grant option;
> flush privileges;
> exit
```

### Running the program

In order to run the program, we must first create the database in our MySQL server. This command is readily available as the recipe `reset_db` in the Makefile. Every time you run this command the database is completely reset to its initial values.
```
cat assets/profiles.sql | mysql -u root
```

### Using the MySQL API for C
Refer to the following link:
https://dev.mysql.com/doc/refman/8.0/en/c-api-function-overview.html

### Latex setup
In order to install latexmk, simply run:
```
sudo apt-get install latexmk
```
Note that `make build` automatically compiles the pdf. In order to edit the .tex documents, the suggested approach is to run the `write-report` recipe in a separate shell, which automatically updates the pdf document as you save the any latex source file. For Linux distros, the suggested pdf viewer is `evince`, which can be configured as the default viewer with the following command:
```
echo "$pdf_previewer = 'evince';" > ~/.latexmkrc
```
