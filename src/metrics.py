import os
import subprocess

import pylab
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

# Test params
iterations = 120
host = '100.112.108.30'
user = 'rafael'
remote_dir = '/home/rafael/Documents/unicamp/courses/mc833/project-1'

# If true, always tries to plot data, otherwise, only plots if in localhost
plot = True

# Filenames
dummy_fn = "bin/dummy.txt"
log_dir = "logs"
client_fn = "cli_time.txt"
database_fn = "proc_time.txt"
stats_fn = "stats.txt"

cities = [
        'Campinas',
        'Berlin',
        'Ouroeste'
        ]
courses = [
        'Computer Engineering',
        'Archeology'
        ]
emails = [
        'juja.princess@gmail.com',
        'panda.prudencio@gmail.com',
        'mimao.vendra@hotmail.com',
        'tutti.nervouser@yahoo.com'
        ]

experiences = [
        'This is a sample experience',
        'This is another experience',
        'More experience!'
        ]

def create_dummy_input_single(option):
    i = 0
    with open(dummy_fn, "w") as f:
        f.write(f"{option}\n")

        if option == 1:
            cur_course = courses[i%len(courses)]
            f.write(f"{cur_course}\n")

        elif option == 2:
            cur_city = cities[i%len(cities)]
            f.write(f"{cur_city}\n")

        elif option == 3:
            cur_email = emails[i%len(emails)]
            cur_exp = experiences[i%len(experiences)]
            f.write(f"{cur_email}\n")
            f.write(f"{cur_exp}\n")

        elif option == 4 or option == 6:
            cur_email = emails[i%len(emails)]
            f.write(f"{cur_email}\n")

        f.write("0\n");

def write_proc_times(option, n, host='localhost'):

    # Create dummy file with a single request
    create_dummy_input_single(option)

    # Compute n processing times
    for i in range(n):
        cmd = f"cat {dummy_fn} | make client ARGS={host}"
        subprocess.run(cmd, shell=True)

    old_client_fn = os.path.join(log_dir, client_fn)
    old_database_fn = os.path.join(log_dir, database_fn)

    if os.path.isfile(old_client_fn):
        new_client_fn = os.path.join(log_dir, f'{option}-{client_fn}')
        os.rename(old_client_fn, new_client_fn)

    new_database_fn = os.path.join(log_dir, f'{option}-{database_fn}')
    if host == 'localhost':
        os.rename(old_database_fn, new_database_fn)
        subprocess.run('make reset_db', shell=True)
    else:
        cmd = f"ssh {user}@{host} 'mv {os.path.join(remote_dir, old_database_fn)} {os.path.join(remote_dir, new_database_fn)}'"
        subprocess.run(cmd, shell=True)

        # Reset database since we might've added a lot of experiences.
        cmd = f"ssh {user}@{host} 'cd {remote_dir}; make reset_db'"
        subprocess.run(cmd, shell=True)

def get_proc_times(option):
    new_client_fn = os.path.join(log_dir, f'{option}-{client_fn}')
    new_database_fn = os.path.join(log_dir, f'{option}-{database_fn}')

    with open(new_client_fn, "r") as f:
        cl_times = f.readlines()
        cl_times = [float(cl.split(' ')[-2]) for cl in cl_times]

    with open(new_database_fn, "r") as f:
        db_times = f.readlines()
        db_times = [float(db.split(' ')[-2]) for db in db_times]

    return np.array(db_times), np.array(cl_times)

def plot_gaussian(proc_times, option, name, log):

    # Remove first and last few elements to eliminate noise
    # proc_times.sort()
    # split = min(len(proc_times)//10, 10)
    # proc_times = proc_times[split:-split]

    # Compute sample mean and sample standard deviation estimate
    mu = np.mean(proc_times)
    sigma = stats.tstd(proc_times)
    interv = (mu-sigma*1.96, mu+sigma*1.96)

    # Get plot filename from plot title.
    if name == 'Tempo de Comunicação':
        plt_name = 'communication'
    elif name == 'Tempo do Cliente':
        plt_name = 'client'
    elif name == 'Tempo de Processamento':
        plt_name = 'proc'

    # plt_name = '_'.join(map(str.lower, name.split(' ')))

    # Create just one figure and one subplot
    fig, ax = plt.subplots(figsize=(10,10))

    # Plot pdf relative likelihood for each point
    pdf = stats.norm.pdf(proc_times, mu, sigma)
    ax.scatter(proc_times, pdf, color=(1.0, 0.0, 0.0, 1.0))

    # Clip min and max y values
    ax.set_ylim(0, 1.1*max(pdf))

    # Create a new curve that represents a 95% confidence interval
    ci_xs = np.linspace(interv[0], interv[1], 30)
    ci_ys = stats.norm.pdf(ci_xs, mu, sigma)
    ax.fill_between(ci_xs, ci_ys, facecolor=(1.0, 0.0, 0.0, 0.4), label='95% IC')

    ax.legend()
    ax.set_title(f'{name} para Opção {option}')
    ax.set_ylabel('$f(x)$')
    ax.set_xlabel(f'{name} (ms)')
    fig.savefig(f'bin/{option}-{plt_name}.pdf')

    # Prints to log file if required
    if log:
        with open(os.path.join(log_dir, stats_fn), 'a') as st:
            st.write(("Option {} - ".format(option) + name + '\n'))
            st.write("Mean: {}".format(mu) + '\n')
            st.write("Standard Deviation: {}".format(sigma) + '\n')
            st.write("Interval: {}".format(interv) + '\n')
            st.write("\n")

def plot_data(option):

    db_times, cl_times = get_proc_times(option)

    plot_gaussian(cl_times-db_times, option, name="Tempo de Comunicação", log=True)
    plot_gaussian(cl_times, option, name="Tempo do Cliente", log=True)
    plot_gaussian(db_times, option, name="Tempo de Processamento", log=True)

def plot_stderr(type='client'):
    filename = os.path.join(log_dir, stats_fn)
    # filename = 'report/data/stats-cut.txt'

    # Store 3-uple with mean, lower, and upper interval
    xs = []
    ys = []
    yerrs = []

    if type == 'client':
        start = 0
    elif type == 'communication':
        start = 5
    else:
        start = 10

    # Create list of stats and options
    with open(filename, 'r') as f:
        lines = f.readlines()
        for i in range(start, len(lines), 15):
            mean = float(lines[i+1].split(':')[-1])
            sigma = float(lines[i+2].split(':')[-1])
            xs.append(i//15 + 1)
            ys.append(mean)
            yerrs.append(sigma*1.96)

    ys = np.array(ys)
    yerrs = np.array(yerrs)

    # Plot graph with given stats
    fig, ax = plt.subplots()
    ax.errorbar(xs, ys, yerrs, linestyle='None', ecolor=(1.0, 0.0, 0.0, 0.5))
    ax.scatter(xs, ys)
    ax.set_ylim(0, 1.1*np.max(ys+yerrs))
    ax.set_ylabel('Tempo de Resposta (ms)')
    ax.set_xlabel('Operação')
    fig.savefig(f'bin/{type}-stderr.pdf')

if __name__ == "__main__":
    # Clear logs
    if host == 'localhost':
        subprocess.run("rm logs/*", shell=True)
        for option in range(1, 7):
            # Write all processing times to separate files
            write_proc_times(option, iterations, host)
            # Compute time metrics and plot data for option
            plot_data(option)
    else:
        if not plot:
            subprocess.run("rm logs/*", shell=True)
        for option in range(1, 7):
            # Write all processing times to separate files
            if plot:
                # Compute time metrics and plot data for option
                plot_data(option)

            else:
                write_proc_times(option, iterations, host)
        if plot:
            plot_stderr('client')
            plot_stderr('communication')
