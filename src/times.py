# times.py
#
# Auxiliary Python script to facilitate result analysis.
#
# Given 2 log files (one for processing times and another for client times), 
# calculates communication times, and makes a statistical analysis (mean,
# standard deviation, 95% confidence interval) and plots two graphs per operation.
#
# Log files have to exist and have the same size.
# For different formats, change function "read_files".
#
# Not intended to be generic, just a specific tool for a specific format.

import statistics as st
import matplotlib.pyplot as plt
from scipy.stats import norm

def main():
    comm = []
    avg_cli = []
    avg_proc = []
    avg_comm = []
    stdev_cli = []
    stdev_proc = []
    stdev_comm = []
    
    # Reads the text log file.
    cli,proc = readfiles()
        
    # Communication time: client - processing.
    for i in range(len(cli)):
        comm.append([])
        for j in range(len(cli[i])):
            at_comm = cli[i][j] - proc[i][j]
            comm[i].append(at_comm)
            
    # Takes the mean and standard deviation of each option.
    for i in cli:
        i.sort()
        avg_cli.append(st.mean(i))
        stdev_cli.append(st.stdev(i))
    
    for i in proc:
		i.sort()
		avg_proc.append(st.mean(i))
        stdev_proc.append(st.stdev(i))

    for i in comm:
        i.sort()
        avg_comm.append(st.mean(i))
        stdev_comm.append(st.stdev(i))

    # Gives the 95% confidence interval for each option
    interv_cli  = [(i - 2*j, i + 2*j) for i, j in zip(avg_cli, stdev_cli)]
    interv_proc = [(i - 2*j, i + 2*j) for i, j in zip(avg_proc, stdev_proc)]
    interv_comm = [(i - 2*j, i + 2*j) for i, j in zip(avg_comm, stdev_comm)]
    
    # TODO: Possible to have negative values... What to do about them?
    
    printall([cli, proc, comm, avg_cli, stdev_cli, interv_cli, avg_comm, stdev_comm, interv_comm])

    # Standardizing
    for i in range(len(cli)):
        cli[i] = [(j-avg_cli[i])/stdev_cli[i] for j in cli[i]]
        comm[i] = [(j-avg_comm[i])/stdev_comm[i] for j in comm[i]]
    
    # Plotting normal distribution for all operations (client and comm)
    for i in cli:
        title = "Normal Distribution - Client Time Op." + str(cli.index(i) + 1)
        plt.scatter(i, norm.pdf(i))
        plt.xlabel("Client Time (ms)")
        plt.ylabel("Probability")
        plt.title(title)
        plt.show()
        
    for i in comm:
        title = "Normal Distribution - Communication Time Op." + str(comm.index(i) + 1)
        plt.scatter(i, norm.pdf(i))
        plt.xlabel("Communication Time (ms)")
        plt.ylabel("Probability")
        plt.title(title)
        plt.show()


# Reads log files.
# Both files have to exist and have the correct path.
# Works only for the format established on "server.c" and "client.c".
def readfiles():
    cli_times = [[],[],[],[],[],[]]
    proc_times = [[],[],[],[],[],[]]
    
    with open('logs/cli_time.txt') as cli:
        cli_in = cli.readlines()
    
    with open('logs/proc_time.txt') as serv:
        serv_in = serv.readlines()
        
    for i in range(0,len(cli_in),3):
        opt = int(cli_in[i][-2]) - 1
        time= float(cli_in[i+1][13:21]) 
        cli_times[opt].append(time)
    
    for i in range(0,len(serv_in),3):
        opt = int(serv_in[i][-2]) - 1
        time= float(serv_in[i+1][17:25])
        proc_times[opt].append(time)
        
    return cli_times,proc_times

def printall(a):
    for i in a:
        print(i)
        print()

if __name__ == "__main__":
    main()
