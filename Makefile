# Setup files and dirs
OUT_DIR = bin
SRC_DIR = src
INCLUDE_DIR = include
REPORT_DIR = report
CLIENT_DIR = client
LOG_DIR = logs

CLIENT = client
SERVER = server
REPORT = main

# Setup flags and libs
CFLAGS	= -Wall -Wextra -std=c99
CFLAGS	+= -I $(INCLUDE_DIR)
SQLFLAGS = `mysql_config --cflags`
LIBS = -lm
SQLLIBS = `mysql_config --libs`


.PHONY: build client run clean reset_db reset_logs write-report

build: $(OUT_DIR)/$(SERVER).o $(OUT_DIR)/$(CLIENT).o $(REPORT_DIR)/$(REPORT).pdf

client: $(OUT_DIR)/$(CLIENT).o
	./$< $(ARGS)

run: $(OUT_DIR)/$(SERVER).o
	./$<

clean:
	rm -rf $(OUT_DIR)/* $(CLIENT_DIR)/* $(LOG_DIR)/*

write-report: $(REPORT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -jobname=$(OUT_DIR)/$(REPORT)

reset_db:
	cat assets/profiles.sql | mysql -u root

reset_logs:
	rm -rf $(LOG_DIR)/*

$(OUT_DIR)/$(SERVER).o: $(SRC_DIR)/$(SERVER).c $(SRC_DIR)/util.c $(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) $(SQLFLAGS) -o $@ $^ $(LIBS) $(SQLLIBS)

$(OUT_DIR)/$(CLIENT).o: $(SRC_DIR)/$(CLIENT).c $(SRC_DIR)/socket.c
	@mkdir -p $(OUT_DIR) $(CLIENT_DIR) $(LOG_DIR)
	gcc $(CFLAGS) -o $@ $^ $(LIBS)

$(REPORT_DIR)/$(REPORT).pdf: $(REPORT_DIR)/$(REPORT).tex
	latexmk -bibtex -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)
